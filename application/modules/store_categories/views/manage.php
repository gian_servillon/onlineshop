
<p>
	<a style="margin-top: 30px" href="<?php echo base_url() ?>store_categories/create" class="btn btn-primary">Add new category</a>
</p>



<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon white align-justify"></i><span class="break"></span>Existing Categories</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon white wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Category Title</th>
								  <th>Parent Category</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
						  	<?php foreach ($query->result() as $row):
						  		$edit_item_url = base_url(). "store_categories/create/".$row->id;
						  	?>
							<tr>
								<td><?php echo $row->cat_title ?></td>						
								<td class="center">
									<?php echo Modules::run('store_categories/_get_cat_title',$row->parent_cat_id)?>
								</td>
								<td class="center">
									<a class="btn btn-success" href="#">
										<i class="halflings-icon white zoom-in"></i>  
									</a>
									<a class="btn btn-info" href="<?php echo $edit_item_url ?>">
										<i class="halflings-icon white edit"></i>  
									</a>
								</td>
							</tr>
							<?php endforeach ?>
					
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->