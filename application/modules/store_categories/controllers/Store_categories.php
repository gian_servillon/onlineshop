<?php
class Store_categories extends MX_Controller 
{

function __construct() {
parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
}

function _get_cat_title($update_id){

    if ($update_id > 0) {
        $data = $this->fetch_data_from_db($update_id);
        $cat_title = $data['cat_title'];
        return $cat_title;
    }else{
        return "----------";
    }
    
}

function fetch_data_from_post(){
    $data['cat_title'] = $this->input->post('cat_title',true);
    $data['parent_cat_id'] = $this->input->post('parent_cat_id',true);
    return $data;
}

function fetch_data_from_db($update_id){
    (!is_numeric($update_id))? redirect('site_security/not_allowed'): "";

    $query = $this->get_where($update_id);
    foreach($query->result() as $row){
        $data['cat_title'] = $row->cat_title; 
        $data['parent_cat_id'] = $row->parent_cat_id;
    }

    return $data = (!isset($data))?  "" :  $data;
    
}

function _get_dropdown_options($update_id){

    (!is_numeric($update_id))? $update_id = 0: "";

    $options[''] = "Please select...";
    //build an array of all the parent categories
    $mysql_query = "SELECT * from store_categories where parent_cat_id=? and id!=?";
    $query = $this->_custom_query_2($mysql_query, 0, $update_id);
    foreach ($query->result() as $row) {
        $options[$row->id] = $row->cat_title;
    }

    return $options;
}

function create(){
    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();

    $update_id = $this->uri->segment(3);
    $submit = $this->input->post('submit', TRUE);

    if ($submit == "Submit") {
        $this->form_validation->set_rules('cat_title', 'Category Title', 'required|max_length[240]');
      
        if ($this->form_validation->run() == TRUE) {
            $data = $this->fetch_data_from_post();
           
            if (is_numeric($update_id)) {
                //update category details
                $this->_update($update_id, $data);
                $flash_msg = "The category details were successfully updated.";
                $value = '<div class="alert alert-success" role="alert">'.$flash_msg.'</div>';
                $this->session->set_flashdata('item', $value);
                redirect('store_categories/create/'.$update_id); 
            }else{
                //insert
                $this->_insert($data);
                $update_id = $this->get_max();//get the ID of the new item
          
                $flash_msg = "The category was successfully added.";
                $value = '<div class="alert alert-success" role="alert">'.$flash_msg.'</div>';
                $this->session->set_flashdata('item', $value);
                redirect('store_categories/create/'.$update_id); 
            }
        }
    }
    elseif ($submit == "Cancel") {
        redirect('store_categories/manage');
    }

    //check if update or create
    if (is_numeric($update_id) && $submit!="Submit") {
        $data = $this->fetch_data_from_db($update_id);
    }else{
        $data = $this->fetch_data_from_post();
    }

    $data['options'] = $this->_get_dropdown_options($update_id);
    $data['num_dropdown_options'] = count($data['options']);
    $data['update_id'] = $update_id;
    $data['headline'] = (!is_numeric($update_id)) ? "Add New Category" : "Update Category Details";
    $data['view_file'] = "create";
    $data['flash'] = $this->session->userdata('item');
    echo Modules::run('templates/admin',$data);
}

function manage(){
    //load manage view file
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();
    $data['headline'] = "Manage Categories";
    $data['flash'] = $this->session->userdata('item');
    $data['query'] = $this->get('cat_title');
    $data['view_file'] = "manage";
    echo Modules::run('templates/admin',$data);
}

function get($order_by)
{
    $this->load->model('mdl_store_categories');
    $query = $this->mdl_store_categories->get($order_by);
    return $query;
}

function get_with_limit($limit, $offset, $order_by) 
{
    if ((!is_numeric($limit)) || (!is_numeric($offset))) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_categories');
    $query = $this->mdl_store_categories->get_with_limit($limit, $offset, $order_by);
    return $query;
}

function get_where($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_categories');
    $query = $this->mdl_store_categories->get_where($id);
    return $query;
}

function get_where_custom($col, $value) 
{
    $this->load->model('mdl_store_categories');
    $query = $this->mdl_store_categories->get_where_custom($col, $value);
    return $query;
}

function _insert($data)
{
    $this->load->model('mdl_store_categories');
    $this->mdl_store_categories->_insert($data);
}

function _update($id, $data)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_categories');
    $this->mdl_store_categories->_update($id, $data);
}

function _delete($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_categories');
    $this->mdl_store_categories->_delete($id);
}

function count_where($column, $value) 
{
    $this->load->model('mdl_store_categories');
    $count = $this->mdl_store_categories->count_where($column, $value);
    return $count;
}

function get_max() 
{
    $this->load->model('mdl_store_categories');
    $max_id = $this->mdl_store_categories->get_max();
    return $max_id;
}

function _custom_query($mysql_query) 
{
    $this->load->model('mdl_store_categories');
    $query = $this->mdl_store_categories->_custom_query($mysql_query);
    return $query;
}

function _custom_query_1($mysql_query, $one) 
{
    $sql = $mysql_query;
    $query = $this->db->query($sql, array($one));
 
    return $query;
}

function _custom_query_2($mysql_query, $one, $two) 
{
    $sql = $mysql_query;
    $query = $this->db->query($sql, array($one, $two));
 
    return $query;
}

function _custom_query_3($mysql_query, $one, $two, $three) 
{
    $sql = $mysql_query;
    $query = $this->db->query($sql, array($one, $two, $three));
 
    return $query;
}

}