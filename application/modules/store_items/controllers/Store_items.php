<?php
class Store_items extends MX_Controller 
{

function __construct() {
        parent::__construct();

       $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
}

function view($update_id){
    (!is_numeric($update_id))? redirect("site_security/not_allowed"): "";

     //fetch the item details
    $data = $this->fetch_data_from_db($update_id);
    $data['update_id'] = $update_id;
    $data['view_file'] = "view";
    $data['flash'] = $this->session->userdata('item');
    echo Modules::run('templates/public_bootstrap',$data);
}

function _process_delete($update_id){
    //attempt to delete item colors
    echo Modules::run('store_item_colors/delete_for_item', $update_id);
    //attempt to delete item sizes
    echo Modules::run('store_item_sizes/delete_for_item', $update_id);
    //attempt to delete item big pics and small pics
    $data = $this->fetch_data_from_db($update_id);
    $big_pic = $data['big_pic'];
    $small_pic = $data['small_pic'];

    $big_pic_path = './big_pics/'.$big_pic;
    $small_pic_path = './small_pics/'.$small_pic;

    //attempt to remove images
    (file_exists($big_pic_path))? unlink($big_pic_path): "";
    (file_exists($small_pic_path))? unlink($small_pic_path): "";
    
    //delete item record from database store_items
    $this->_delete($update_id);
}

function delete($update_id){
    (!is_numeric($update_id))? redirect("site_security/not_allowed"): "";
    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();

    $submit = $this->input->post('submit', TRUE);

    if ($submit == "Cancel") {
        redirect('store_items/create/'.$update_id);
    }elseif ($submit == "Yes - Delete Item") {
        $this->_process_delete($update_id);
        $flash_msg = "The item was successfully deleted.";
        $value = '<div class="alert alert-success" role="alert">'.$flash_msg.'</div>';
        $this->session->set_flashdata('item', $value);

        redirect('store_items/manage');
    }

    
}

function deleteconf($update_id){
    (!is_numeric($update_id))? redirect("site_security/not_allowed"): "";
    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();

    $data['update_id'] = $update_id;
    $data['headline'] = "Delete Item";
    $data['view_file'] = "deleteconf";
    $data['flash'] = $this->session->userdata('item');
    echo Modules::run('templates/admin',$data);
}

function delete_image($update_id){
    (!is_numeric($update_id))? redirect("site_security/not_allowed"): "";
    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();

    $data = $this->fetch_data_from_db($update_id);
    $big_pic = $data['big_pic'];
    $small_pic = $data['small_pic'];

    $big_pic_path = './big_pics/'.$big_pic;
    $small_pic_path = './small_pics/'.$small_pic;

    //attempt to remove images
    (file_exists($big_pic_path))? unlink($big_pic_path): "";
    (file_exists($small_pic_path))? unlink($small_pic_path): "";

    //update database
    unset($data);
    $data['big_pic'] = "";
    $data['small_pic'] = "";
    $this->_update($update_id, $data);

    $flash_msg = "The item image was successfully deleted.";
    $value = '<div class="alert alert-success" role="alert">'.$flash_msg.'</div>';
    $this->session->set_flashdata('item', $value);

    redirect('store_items/create/'.$update_id);
}

function _generate_thumbnail($file_name){
    $config['image_library'] = 'gd2';
    $config['source_image'] = './big_pics/'.$file_name;
    $config['new_image'] = './small_pics/'.$file_name;
    $config['maintain_ratio'] = TRUE;
    $config['width']         = 200;
    $config['height']       = 200;

    $this->load->library('image_lib', $config);

    $this->image_lib->resize();
}

function do_upload($update_id){
    (!is_numeric($update_id))? redirect("site_security/not_allowed"): "";
    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();

    $submit = $this->input->post('submit',TRUE);

    ($submit == 'Cancel')? redirect('store_items/create/'.$update_id): "";

    $config['upload_path']          = './big_pics/';
    $config['allowed_types']        = 'gif|jpg|png';
    $config['max_size']             = 100;
    $config['max_width']            = 1024;
    $config['max_height']           = 768;

    $this->load->library('upload', $config);

    if ( ! $this->upload->do_upload('userfile'))
    {
        $data['error'] = array('error' => $this->upload->display_errors("<p style='color:red;'>","</p>"));
        $data['update_id'] = $update_id;
        $data['headline'] = "Upload Error";
        $data['view_file'] = "upload_image";
        $data['flash'] = $this->session->userdata('item');
        echo Modules::run('templates/admin',$data);
    }
    else
    {
        $data = array('upload_data' => $this->upload->data());

        $upload_data = $data['upload_data'];
        $file_name = $upload_data['file_name'];

        $this->_generate_thumbnail($file_name);

        //update database
        $update_data['big_pic'] = $file_name;
        $update_data['small_pic'] = $file_name;
        $this->_update($update_id, $update_data);

        $data['update_id'] = $update_id;
        $data['headline'] = "Upload Success";
        $data['view_file'] = "upload_success";
        $data['flash'] = $this->session->userdata('item');
        echo Modules::run('templates/admin',$data);
            

            //$this->load->view('upload_success', $data);
    }
}

function upload_image($update_id){
    (!is_numeric($update_id))? redirect("site_security/not_allowed"): "";
    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();

    $update_id = $this->uri->segment(3);
    $submit = $this->input->post('submit', TRUE);

    

    $data['update_id'] = $update_id;
    $data['headline'] = "Upload Image";
    $data['view_file'] = "upload_image";
    $data['flash'] = $this->session->userdata('item');
    echo Modules::run('templates/admin',$data);
}


function create(){
    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();

    $update_id = $this->uri->segment(3);
    $submit = $this->input->post('submit', TRUE);

    if ($submit == "Submit") {
        $this->form_validation->set_rules('item_title', 'Item Title', 'required|max_length[240]|callback_item_check');
        $this->form_validation->set_rules('item_price', 'Item Price', 'required|numeric');
        $this->form_validation->set_rules('was_price', 'Was Price', 'numeric');
        $this->form_validation->set_rules('status', 'Status', 'required|numeric');
        $this->form_validation->set_rules('item_description', 'Item Description', 'required');

        if ($this->form_validation->run() == TRUE) {
            $data = $this->fetch_data_from_post();
            //url friendly
            $data['item_url'] = url_title($data['item_title']);
            if (is_numeric($update_id)) {
                //update
                $this->_update($update_id, $data);
                $flash_msg = "The item details were successfully updated.";
                $value = '<div class="alert alert-success" role="alert">'.$flash_msg.'</div>';
                $this->session->set_flashdata('item', $value);
                redirect('store_items/create/'.$update_id); 
            }else{
                //insert
                $this->_insert($data);
                $update_id = $this->get_max();//get the ID of the new item
          
                $flash_msg = "The item was successfully added.";
                $value = '<div class="alert alert-success" role="alert">'.$flash_msg.'</div>';
                $this->session->set_flashdata('item', $value);
                redirect('store_items/create/'.$update_id); 
            }
        }
    }
    elseif ($submit == "Cancel") {
        redirect('store_items/manage');
    }

    //check if update or create
    if (is_numeric($update_id) && $submit!="Submit") {
        $data = $this->fetch_data_from_db($update_id);
    }else{
        $data = $this->fetch_data_from_post();
        $data['big_pic'] = "";
    }

    $data['update_id'] = $update_id;
    $data['headline'] = (!is_numeric($update_id)) ? "Add New Item" : "Update Item Details";
    $data['view_file'] = "create";
    $data['flash'] = $this->session->userdata('item');
    echo Modules::run('templates/admin',$data);
}



function manage(){
    //load manage view file
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();
    $data['headline'] = "Manage Items";
    $data['flash'] = $this->session->userdata('item');
    $data['query'] = $this->get('item_title');
    $data['view_file'] = "manage";
    echo Modules::run('templates/admin',$data);
}

function fetch_data_from_post(){
    $data['item_title'] = $this->input->post('item_title',true);
    $data['item_price'] = $this->input->post('item_price',true);
    $data['was_price'] = $this->input->post('was_price',true);
    $data['item_description'] = $this->input->post('item_description',true);
    $data['status'] = $this->input->post('status',true);
    return $data;
}

function fetch_data_from_db($update_id){
    (!is_numeric($update_id))? redirect('site_security/not_allowed'): "";

    $query = $this->get_where($update_id);
    foreach($query->result() as $row){
        $data['item_title'] = $row->item_title;
        $data['item_url'] = $row->item_url;
        $data['item_price'] = $row->item_price;
        $data['item_description'] = $row->item_description;
        $data['big_pic'] = $row->big_pic;
        $data['small_pic'] = $row->small_pic;
        $data['was_price'] = $row->was_price;
        $data['status'] = $row->status;
    }

    return $data = (!isset($data))?  "" :  $data;
    
}

function get($order_by)
{
    $this->load->model('mdl_store_items');
    $query = $this->mdl_store_items->get($order_by);
    return $query;
}

function get_with_limit($limit, $offset, $order_by) 
{
    if ((!is_numeric($limit)) || (!is_numeric($offset))) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_items');
    $query = $this->mdl_store_items->get_with_limit($limit, $offset, $order_by);
    return $query;
}

function get_where($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_items');
    $query = $this->mdl_store_items->get_where($id);
    return $query;
}

function get_where_custom($col, $value) 
{
    $this->load->model('mdl_store_items');
    $query = $this->mdl_store_items->get_where_custom($col, $value);
    return $query;
}

function _insert($data)
{
    $this->load->model('mdl_store_items');
    $this->mdl_store_items->_insert($data);
}

function _update($id, $data)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_items');
    $this->mdl_store_items->_update($id, $data);
}

function _delete($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_items');
    $this->mdl_store_items->_delete($id);
}

function count_where($column, $value) 
{
    $this->load->model('mdl_store_items');
    $count = $this->mdl_store_items->count_where($column, $value);
    return $count;
}

function get_max() 
{
    $this->load->model('mdl_store_items');
    $max_id = $this->mdl_store_items->get_max();
    return $max_id;
}

function _custom_query($mysql_query) 
{
    $this->load->model('mdl_store_items');
    $query = $this->mdl_store_items->_custom_query($mysql_query);
    return $query;
}

function _custom_query_1($mysql_query, $one) 
{
    $sql = $mysql_query;
    $query = $this->db->query($sql, array($one));
 
    return $query;
}

function _custom_query_2($mysql_query, $one, $two) 
{
    $sql = $mysql_query;
    $query = $this->db->query($sql, array($one, $two));
 
    return $query;
}

function _custom_query_3($mysql_query, $one, $two, $three) 
{
    $sql = $mysql_query;
    $query = $this->db->query($sql, array($one, $two, $three));
 
    return $query;
}

function item_check($str){
    $item_url = url_title($str);
    $mysql_query = "SELECT * from store_items where item_title = ? and item_url = ? ";

    $update_id = $this->uri->segment(3);


    

   if (is_numeric($update_id)) {
       $mysql_query .= " and id != ?";
       $query = $this->_custom_query_3($mysql_query, "$str", "$item_url", $update_id);
   }else{
       $query = $this->_custom_query_2($mysql_query,"$str", "$item_url");
   }

   $num_rows = $query->num_rows();
   // echo $num_rows;die;
   if ($num_rows>0) {
       $this->form_validation->set_message('item_check','The item title that you submitted is not available');
       return FALSE;
   }else{
       return TRUE;
   }
}

// function item_check($str){

//     if ($str == 'test')
//     {
//             $this->form_validation->set_message('item_check', 'The {field} field can not be the word "test"');
//             return FALSE;
//     }
//     else
//     {
//             return TRUE;
//     }
// }

}