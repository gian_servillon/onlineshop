
<?php if (is_numeric($update_id)): ?>
	

<div class="row-fluid sortable ui-sortable">
				<div class="box span12">
					<div class="box-header" data-original-title="">
						<h2><i class="halflings-icon white edit"></i><span class="break"></span>Item Options</h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
					<?php if ($big_pic == ""): ?>
						<a href="<?php echo base_url() ?>store_items/upload_image/<?php echo $update_id ?>" class="btn btn-primary">Upload Item Images</a>
					<?php else: ?>
						<a href="<?php echo base_url() ?>store_items/delete_image/<?php echo $update_id ?>" class="btn btn-danger">Delete Item Image</a>
					<?php endif ?>
					
					<a href="<?php echo base_url() ?>store_item_colors/update/<?php echo $update_id ?>" class="btn btn-primary">Update Item Colors</a>
					<a href="<?php echo base_url() ?>store_item_sizes/update/<?php echo $update_id ?>" class="btn btn-primary">Update Item Sizes</a>
					<a href="" class="btn btn-primary">Upload Item Categories</a>
					<a href="<?php echo base_url() ?>store_items/deleteconf/<?php echo $update_id ?>" class="btn btn-danger">Delete Item</a>
					<a href="<?php echo base_url() ?>store_items/view/<?php echo $update_id ?>" class="btn btn-default">View Item in Shop</a>

					</div>
				</div><!--/span-->

			</div>
<?php endif ?>

<div class="row-fluid sortable ui-sortable">
				<div class="box span12">
					<div class="box-header" data-original-title="">
						<h2><i class="halflings-icon white edit"></i><span class="break"></span>Form Elements</h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?php $form_location = base_url(). "store_items/create/".$update_id ?>
						<form class="form-horizontal" method="post" action="<?php echo $form_location?>"
						  <fieldset>
							<div class="control-group">
							  <label class="control-label" for="typeahead">Item Title </label>
							  <div class="controls">
								<input type="text" class="span6" name="item_title" value="<?php echo $item_title ?>">							
							  </div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="typeahead">Item Price </label>
							  <div class="controls">
								<input type="text" class="span1" name="item_price" value="<?php echo $item_price ?>">							
							  </div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="typeahead">Was Price <span style="color:green">(optional)</span></label>
							  <div class="controls">
								<input type="text" class="span1" name="was_price" value="<?php echo $was_price ?>">							
							  </div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="typeahead">Status</label>
							  <div class="controls">
							  <?php 

							  	$addtional_dd_code = 'id="selectError3"';
							  	$options = array(
							  		''  => "Please select...",
							  		'0' => 'Inactive',
							  		'1' => 'Active'
							  	);

							   echo form_dropdown('status', $options, $status, $addtional_dd_code);
							   ?>			
							  </div>
							</div>
					       
							<div class="control-group hidden-phone">
								  <label class="control-label" for="textarea2">Item Description</label>
								  <div class="controls">
									<textarea class="cleditor" id="textarea2" rows="3" name="item_description"><?php echo $item_description ?></textarea>
								  </div>
							</div>
							<div class="form-actions">
							  <button type="submit" class="btn btn-primary" name="submit" value="Submit">Save changes</button>
							  <button type="submit" class="btn" name="submit" value="Cancel">Cancel</button>
							</div>
						  </fieldset>
						</form>   

					</div>
				</div><!--/span-->

			</div>

<?php if ($big_pic !== ""): ?>
	

<div class="row-fluid sortable ui-sortable">
				<div class="box span12">
					<div class="box-header" data-original-title="">
						<h2><i class="halflings-icon white edit"></i><span class="break"></span>Item Image</h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
					<?php $big_pic_address = base_url().'big_pics/'.$big_pic ?>
					<img src="<?php echo $big_pic_address ?>" alt="">

					</div>
				</div><!--/span-->

			</div>
<?php endif ?>