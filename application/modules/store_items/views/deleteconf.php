<div class="row-fluid sortable ui-sortable">
	<div class="box span12">
		<div class="box-header" data-original-title="">
			<h2><i class="halflings-icon white edit"></i><span class="break"></span>Delete Item</h2>
			<div class="box-icon">
				
				<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
				<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
			</div>
		</div>
		<div class="box-content">
			
			<p>Are you sure you want to delete the item?</p>
			
			<?php 
			echo form_open('store_items/delete/'.$update_id);
					?>
						
		<fieldset>
			<div class="control-group" style="height: 200px">			
				<div class="controls">
					<button type="submit" name="submit" value="Yes - Delete Item" class="btn btn-danger">Yes - Delete Item</button>
					<button type="submit" name="submit" value="Cancel" class="btn btn-default">Cancel</button>
				</div>
			</div>
			
			
		</fieldset>
		</form>
						

		</div>
	</div><!--/span-->

</div>