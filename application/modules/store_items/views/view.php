<div class="row">
	<div class="col-md-4" style="margin-top: 24px">
	<img src="<?php echo base_url() ?>big_pics/<?php echo $big_pic ?>" class="img-responsive" alt="<?php echo $item_title ?>">
	</div>
	<div class="col-md-5">
		<h1><?php echo $item_title ?></h1>
		<div style="clear: both">
			<?php echo nl2br($item_description) ?>
		</div>
	</div>
	<div class="col-md-3">
	<?php echo Modules::run('cart/_draw_add_to_cart',$update_id) ?>
	</div>
</div>
