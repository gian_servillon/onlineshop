<div class="row-fluid sortable ui-sortable">
				<div class="box span12">
					<div class="box-header" data-original-title="">
						<h2><i class="halflings-icon white edit"></i><span class="break"></span>Upload Image</h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
					<?php 
					if (isset($error)) {
						foreach ($error as $value) {
							echo $value;
						}
					}

					$attrubutes = array('class' => 'form-horizontal');
					echo form_open_multipart('store_items/do_upload/'.$update_id,$attrubutes);
					?>
					<p style="margin-top: 24px;">Please choose a file from your computer and then press 'Upload'</p>
					<fieldset>
						<div class="control-group" style="height: 200px">
							<label for="fileinput" class="control-label">file input</label>
							<div class="controls">
								<input type="file" name="userfile" id="fileInput" class="input-file uniform_on">
							</div>
						</div>
						
						<div class="form-actions">
							<button type="submit" name="submit" value="Submit" class="btn btn-primary">Upload</button>
							<button type="submit" name="submit" value="Cancel" class="btn btn-default">Cancel</button>
						</div>
					</fieldset>
					</form>
					</div>
				</div><!--/span-->

</div>