<?php if (is_numeric($update_id)): ?>
	

<div class="row-fluid sortable ui-sortable">
				<div class="box span12">
					<div class="box-header" data-original-title="">
						<h2><i class="halflings-icon white edit"></i><span class="break"></span>Account Options</h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">			
					<a href="<?php echo base_url() ?>store_accounts/update_pword/<?php echo $update_id ?>" class="btn btn-primary">Update Password	</a>
					<a href="<?php echo base_url() ?>store_accounts/deleteconf/<?php echo $update_id ?>" class="btn btn-danger">Delete Account</a>
					
					</div>
				</div><!--/span-->

			</div>
<?php endif ?>


<div class="row-fluid sortable ui-sortable">
				<div class="box span12">
					<div class="box-header" data-original-title="">
						<h2><i class="halflings-icon white edit"></i><span class="break"></span>Account Details</h2>
						<div class="box-icon">
							
							<a href="#" class="btn-minimize"><i class="halflings-icon white chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon white remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<?php $form_location = base_url(). "store_accounts/create/".$update_id ?>
						<form class="form-horizontal" method="post" action="<?php echo $form_location?>"
						  <fieldset>
							<div class="control-group"> <label class="control-label" for="typeahead">Firstname</label> <div class="controls"> <input type="text" class="span6" name="firstname" value="<?php echo $firstname ?>"> </div> </div> 
<div class="control-group"> <label class="control-label" for="typeahead">Lastname</label> <div class="controls"> <input type="text" class="span6" name="lastname" value="<?php echo $lastname ?>"> </div> </div> 
<div class="control-group"> <label class="control-label" for="typeahead">Company</label> <div class="controls"> <input type="text" class="span6" name="company" value="<?php echo $company ?>"> </div> </div> 
<div class="control-group"> <label class="control-label" for="typeahead">Address1</label> <div class="controls"> <input type="text" class="span6" name="address1" value="<?php echo $address1 ?>"> </div> </div> 
<div class="control-group"> <label class="control-label" for="typeahead">Address2</label> <div class="controls"> <input type="text" class="span6" name="address2" value="<?php echo $address2 ?>"> </div> </div> 
<div class="control-group"> <label class="control-label" for="typeahead">Town</label> <div class="controls"> <input type="text" class="span6" name="town" value="<?php echo $town ?>"> </div> </div> 
<div class="control-group"> <label class="control-label" for="typeahead">Country</label> <div class="controls"> <input type="text" class="span6" name="country" value="<?php echo $country ?>"> </div> </div> 
<div class="control-group"> <label class="control-label" for="typeahead">Postcode</label> <div class="controls"> <input type="text" class="span6" name="postcode" value="<?php echo $postcode ?>"> </div> </div> 
<div class="control-group"> <label class="control-label" for="typeahead">Telnum</label> <div class="controls"> <input type="text" class="span6" name="telnum" value="<?php echo $telnum ?>"> </div> </div> 
<div class="control-group"> <label class="control-label" for="typeahead">Email</label> <div class="controls"> <input type="text" class="span6" name="email" value="<?php echo $email ?>"> </div> </div> 

							<div class="form-actions">
							  <button type="submit" class="btn btn-primary" name="submit" value="Submit">Save changes</button>
							  <button type="submit" class="btn" name="submit" value="Cancel">Cancel</button>
							</div>
						  </fieldset>
						</form>   

					</div>
				</div><!--/span-->

			</div>

