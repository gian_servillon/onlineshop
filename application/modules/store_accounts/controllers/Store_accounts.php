<?php
class Store_accounts extends MX_Controller 
{

function __construct() {
parent::__construct();
        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
}

function update_pword(){
    $update_id = $this->uri->segment(3);
    (!is_numeric($update_id))? redirect('site_security/not_allowed'): "";
    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();
 
    $submit = $this->input->post('submit', TRUE);

    if ($submit == "Submit") {
        $this->form_validation->set_rules('pword', 'Password', 'required|min_length[7]|max_length[35]');
        $this->form_validation->set_rules('repeat_pword', 'Repeat Password', 'required|matches[pword]');
       

        if ($this->form_validation->run() == TRUE) {
                $pword = $this->input->post('pword', TRUE);
                $data['pword'] = Modules::run('site_security/_hash_string',$pword);
                //update
                $this->_update($update_id, $data);
                $flash_msg = "The account password were successfully updated.";
                $value = '<div class="alert alert-success" role="alert">'.$flash_msg.'</div>';
                $this->session->set_flashdata('item', $value);
                redirect('store_accounts/create/'.$update_id); 
            
        }
    }
    elseif ($submit == "Cancel") {
        redirect('store_accounts/create/'.$update_id);
    }


    $data['update_id'] = $update_id;
    $data['headline'] = "Update Account Password";
    $data['view_file'] = "update_pword";
    $data['flash'] = $this->session->userdata('item');
    echo Modules::run('templates/admin',$data);
}

function fetch_data_from_post(){
    $data['firstname'] = $this->input->post('firstname', TRUE);
    $data['lastname'] = $this->input->post('lastname', TRUE);
    $data['company'] = $this->input->post('company', TRUE);
    $data['address1'] = $this->input->post('address1', TRUE);
    $data['address2'] = $this->input->post('address2', TRUE);
    $data['town'] = $this->input->post('town', TRUE);
    $data['country'] = $this->input->post('country', TRUE);
    $data['postcode'] = $this->input->post('postcode', TRUE);
    $data['telnum'] = $this->input->post('telnum', TRUE);
    $data['email'] = $this->input->post('email', TRUE);
    
    return $data;
}

function fetch_data_from_db($update_id){
    (!is_numeric($update_id))? redirect('site_security/not_allowed'): "";

    $query = $this->get_where($update_id);
    foreach($query->result() as $row){
        $data['firstname'] = $row->firstname;
        $data['lastname'] = $row->lastname;
        $data['company'] = $row->company;
        $data['address1'] = $row->address1;
        $data['address2'] = $row->address2;
        $data['town'] = $row->town;
        $data['country'] = $row->country;
        $data['postcode'] = $row->postcode;
        $data['telnum'] = $row->telnum;
        $data['email'] = $row->email;
        $data['date_made'] = $row->date_made;
        $data['pword'] = $row->pword;
    }

    return $data = (!isset($data))?  "" :  $data;
    
}

function create(){
    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();

    $update_id = $this->uri->segment(3);
    $submit = $this->input->post('submit', TRUE);

    if ($submit == "Submit") {
        $this->form_validation->set_rules('firstname', 'First Name', 'required');
       

        if ($this->form_validation->run() == TRUE) {
            $data = $this->fetch_data_from_post();
        
            if (is_numeric($update_id)) {
                //update
                $this->_update($update_id, $data);
                $flash_msg = "The account details were successfully updated.";
                $value = '<div class="alert alert-success" role="alert">'.$flash_msg.'</div>';
                $this->session->set_flashdata('item', $value);
                redirect('store_accounts/create/'.$update_id); 
            }else{
                //insert
                $data['date_made'] = time();
                $this->_insert($data);
                $update_id = $this->get_max();//get the ID of the new account
          
                $flash_msg = "The account was successfully added.";
                $value = '<div class="alert alert-success" role="alert">'.$flash_msg.'</div>';
                $this->session->set_flashdata('item', $value);
                redirect('store_accounts/create/'.$update_id); 
            }
        }
    }
    elseif ($submit == "Cancel") {
        redirect('store_accounts/manage');
    }

    //check if update or create
    if (is_numeric($update_id) && $submit!="Submit") {
        $data = $this->fetch_data_from_db($update_id);
    }else{
        $data = $this->fetch_data_from_post();     
    }

    $data['update_id'] = $update_id;
    $data['headline'] = (!is_numeric($update_id)) ? "Add New Account" : "Update Account Details";
    $data['view_file'] = "create";
    $data['flash'] = $this->session->userdata('item');
    echo Modules::run('templates/admin',$data);
}

function manage(){
    //load manage view file
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();
    $data['headline'] = "Manage accounts";
    $data['flash'] = $this->session->userdata('item');
    $data['query'] = $this->get('lastname');
    $data['view_file'] = "manage";
    echo Modules::run('templates/admin',$data);
}

function get($order_by)
{
    $this->load->model('mdl_store_accounts');
    $query = $this->mdl_store_accounts->get($order_by);
    return $query;
}

function get_with_limit($limit, $offset, $order_by) 
{
    if ((!is_numeric($limit)) || (!is_numeric($offset))) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_accounts');
    $query = $this->mdl_store_accounts->get_with_limit($limit, $offset, $order_by);
    return $query;
}

function get_where($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_accounts');
    $query = $this->mdl_store_accounts->get_where($id);
    return $query;
}

function get_where_custom($col, $value) 
{
    $this->load->model('mdl_store_accounts');
    $query = $this->mdl_store_accounts->get_where_custom($col, $value);
    return $query;
}

function _insert($data)
{
    $this->load->model('mdl_store_accounts');
    $this->mdl_store_accounts->_insert($data);
}

function _update($id, $data)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_accounts');
    $this->mdl_store_accounts->_update($id, $data);
}

function _delete($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_accounts');
    $this->mdl_store_accounts->_delete($id);
}

function count_where($column, $value) 
{
    $this->load->model('mdl_store_accounts');
    $count = $this->mdl_store_accounts->count_where($column, $value);
    return $count;
}

function get_max() 
{
    $this->load->model('mdl_store_accounts');
    $max_id = $this->mdl_store_accounts->get_max();
    return $max_id;
}

function _custom_query($mysql_query) 
{
    $this->load->model('mdl_store_accounts');
    $query = $this->mdl_store_accounts->_custom_query($mysql_query);
    return $query;
}

function _custom_query_1($mysql_query, $one) 
{
    $sql = $mysql_query;
    $query = $this->db->query($sql, array($one));
 
    return $query;
}

function _custom_query_2($mysql_query, $one, $two) 
{
    $sql = $mysql_query;
    $query = $this->db->query($sql, array($one, $two));
 
    return $query;
}

function _custom_query_3($mysql_query, $one, $two, $three) 
{
    $sql = $mysql_query;
    $query = $this->db->query($sql, array($one, $two, $three));
 
    return $query;
}


function autogen(){
    // $this->load->module('site_security');
    // $this->site_security->_make_sure_is_admin();

    $mysql_query = "show columns from store_accounts";
    $query = $this->_custom_query($mysql_query);
    foreach ($query->result() as $row) {
        $column_name = $row->Field;
        if ($column_name !== 'id') {
            echo '$data[\''.$column_name.'\'] = $this->input->post(\''.$column_name.'\', TRUE);<br>';
        }
         
    }
    echo "<hr>";

    foreach ($query->result() as $row) {
        $column_name = $row->Field;
        if ($column_name !== 'id') {
            echo '$data[\''. $column_name.'\'] = $row->'.$column_name.';<br>';
        }
         
    }

    echo "<hr>";

        foreach ($query->result() as $row) {
            $column_name = $row->Field;
            if ($column_name !== 'id') {
                 $var = '<div class="control-group">
                          <label class="control-label" for="typeahead">'.ucfirst($column_name).'</label>
                          <div class="controls">
                            <input type="text" class="span6" name="'.$column_name.'" value="<?php echo $'.$column_name.' ?>">
                          </div>
                        </div> ';     

                echo htmlentities($var);
                echo "<br>";
            }

        }

}

}