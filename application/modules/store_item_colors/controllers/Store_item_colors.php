<?php
class Store_item_colors extends MX_Controller 
{

function __construct() {
parent::__construct();
    $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
}

function delete_for_item($update_id){
    $mysql_query = "DELETE from store_item_colors where item_id = ?";
    $query = $this->_custom_query_1($mysql_query, $update_id);
}

function delete($update_id){
    (!is_numeric($update_id))? redirect("site_security/not_allowed"): "";
    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();

    $query = $this->get_where($update_id);
    foreach ($query->result() as $row) {
        $item_id = $row->item_id;
    }

    $this->_delete($update_id);
     $flash_msg = "The item color was successfully deleted.";
    $value = '<div class="alert alert-success" role="alert">'.$flash_msg.'</div>';
    $this->session->set_flashdata('item', $value);
    redirect('store_item_colors/update/'.$item_id);
}

function submit($update_id){
    (!is_numeric($update_id))? redirect("site_security/not_allowed"): "";
    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();

    $submit = $this->input->post('submit', TRUE);
    $color = $this->input->post('color', TRUE);

    if ($submit == "Finished") {
        redirect('store_items/create/'.$update_id);
    }elseif ($submit == "Submit"){
        $this->form_validation->set_rules('color', 'Color', 'trim|required');

        if ($this->form_validation->run() == TRUE) {
            $data['item_id'] = $update_id;
            $data['color'] = $color;
            $this->_insert($data);

            $flash_msg = "The item color were successfully added.";
            $value = '<div class="alert alert-success" role="alert">'.$flash_msg.'</div>';
            $this->session->set_flashdata('item', $value);
            redirect('store_item_colors/update/'.$update_id);
        }else{
             $data['query'] = $this->get_where_custom('item_id', $update_id);
            
            $data['update_id'] = $update_id;
            $data['headline'] = "Update Item Colors";
            $data['view_file'] = "update";
            $data['flash'] = $this->session->userdata('item');
            echo Modules::run('templates/admin',$data);
        }
    }
}

function update($update_id){
    (!is_numeric($update_id))? redirect("site_security/not_allowed"): "";
    $this->load->library('session');
    $this->load->module('site_security');
    $this->site_security->_make_sure_is_admin();

    $update_id = $this->uri->segment(3);
    
    //fetch existing colors
    $data['query'] = $this->get_where_custom('item_id', $update_id);

    $data['update_id'] = $update_id;
    $data['headline'] = "Update Item Colors";
    $data['view_file'] = "update";
    $data['flash'] = $this->session->userdata('item');
    echo Modules::run('templates/admin',$data);
}

function get($order_by)
{
    $this->load->model('mdl_store_item_colors');
    $query = $this->mdl_store_item_colors->get($order_by);
    return $query;
}

function get_with_limit($limit, $offset, $order_by) 
{
    if ((!is_numeric($limit)) || (!is_numeric($offset))) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_item_colors');
    $query = $this->mdl_store_item_colors->get_with_limit($limit, $offset, $order_by);
    return $query;
}

function get_where($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_item_colors');
    $query = $this->mdl_store_item_colors->get_where($id);
    return $query;
}

function get_where_custom($col, $value) 
{
    $this->load->model('mdl_store_item_colors');
    $query = $this->mdl_store_item_colors->get_where_custom($col, $value);
    return $query;
}

function _insert($data)
{
    $this->load->model('mdl_store_item_colors');
    $this->mdl_store_item_colors->_insert($data);
}

function _update($id, $data)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_item_colors');
    $this->mdl_store_item_colors->_update($id, $data);
}

function _delete($id)
{
    if (!is_numeric($id)) {
        die('Non-numeric variable!');
    }

    $this->load->model('mdl_store_item_colors');
    $this->mdl_store_item_colors->_delete($id);
}

function count_where($column, $value) 
{
    $this->load->model('mdl_store_item_colors');
    $count = $this->mdl_store_item_colors->count_where($column, $value);
    return $count;
}

function get_max() 
{
    $this->load->model('mdl_store_item_colors');
    $max_id = $this->mdl_store_item_colors->get_max();
    return $max_id;
}

function _custom_query($mysql_query) 
{
    $this->load->model('mdl_store_item_colors');
    $query = $this->mdl_store_item_colors->_custom_query($mysql_query);
    return $query;
}

function _custom_query_1($mysql_query, $one) 
{
    $sql = $mysql_query;
    $query = $this->db->query($sql, array($one));
 
    return $query;
}

function _custom_query_2($mysql_query, $one, $two) 
{
    $sql = $mysql_query;
    $query = $this->db->query($sql, array($one, $two));
 
    return $query;
}

function _custom_query_3($mysql_query, $one, $two, $three) 
{
    $sql = $mysql_query;
    $query = $this->db->query($sql, array($one, $two, $three));
 
    return $query;
}

}