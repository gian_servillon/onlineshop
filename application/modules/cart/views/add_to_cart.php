<div style="background-color: #ddd; border-radius: 7px; margin-top: 24px; padding: 7px">
	<table class="table">
		<tr>
			<td colspan="2">Item ID: <?php echo $item_id ?></td>
		</tr>
		<?php if ($num_colors > 0): ?>
		<tr>
			<td>Color: </td>
			<td>
			<?php 

		  	$addtional_dd_code = 'class="form-control"';
			echo form_dropdown('status', $color_options, $submitted_color, $addtional_dd_code);
		   ?>
			</td>
		</tr>
		<?php endif ?>
		<?php if ($num_sizes > 0): ?>
		<tr>
			<td>Size: </td>
			<td>
			<?php 

		  	$addtional_dd_code = 'class="form-control"';
			echo form_dropdown('status', $size_options, $submitted_size, $addtional_dd_code);
		   ?>
			</td>
		</tr>
		<?php endif ?>
		<tr>
			<td>Qty: </td>
			<td>
				<div class="col-sm-5" style="padding: 0px">
					<input type="text" class="form-control">
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center">
			<button class="btn btn-primary">
			<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
			Add To Basket</button>
			</td>
		</tr>
	</table>
</div>